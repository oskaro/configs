#!/usr/bin/env bash

CMD="$1"

if [[ "$CMD" = "up" ]]; then
    sudo mount -t cifs -o username=oskaro,vers=2.0,rw,uid=`id -u`,gid=`id -g` //della.norbit.no/norbit /export/norbit
    sudo mount -t cifs -o username=oskaro,vers=2.0,rw,uid=`id -u`,gid=`id -g` //della.norbit.no/oskaro /export/home
elif [[ "$CMD" = "down" ]]; then
    sudo umount -q /export/norbit
    sudo umount -q /export/home
else
    echo "Unrecognized command '$1'"
    echo
    echo "    Usage: $0 [up|down]"
fi

