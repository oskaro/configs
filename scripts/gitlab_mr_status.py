#!/usr/bin/env python3

from dataclasses import dataclass, field
import os
import subprocess
import gitlab
import click
import json
import sys
from subprocess import run
from xdg_base_dirs import xdg_config_home, xdg_cache_home

from pathlib import Path

# See the documentation for python-gitlab
PACKAGE_XDG_NAME = "glmrf"
CONFIG_DIR: Path = xdg_config_home() / PACKAGE_XDG_NAME
CONFIG_FILE = CONFIG_DIR / "glmrf.cfg"
MR_STATE_FILE = xdg_cache_home() /  Path(PACKAGE_XDG_NAME, "mr_state.json")
ERRMSG_NO_CONFIG = f"""Cannot load config file, please write it to:

    {CONFIG_FILE}

The format can be found here: https://python-gitlab.readthedocs.io/en/latest/cli-usage.html#configuration-file-format
"""

@dataclass
class MRState:
    assigned_to_me: set[int] = field(default_factory=set)
    review_requested: set[int] = field(default_factory=set)

def get_gitlab_instance() -> gitlab.Gitlab:
    if not CONFIG_FILE.exists():
        print(ERRMSG_NO_CONFIG)
        sys.exit(1)

    # TODO move this instance id to arguments.
    gl = gitlab.Gitlab.from_config("gitdev.norbit.com", [str(CONFIG_FILE)])

    return gl


def read_mr_state() -> MRState:
    if not MR_STATE_FILE.exists():
        return MRState()

    with MR_STATE_FILE.open("r") as fh:
        mr_state = json.load(fh)
    try:
        return MRState(set(mr_state["assigned_to_me"]), set(mr_state["review_requested"]))
    except:
        print("Could not parse MR state file")
        return MRState()

def write_mr_state(state: MRState):
    mr_state = dict(assigned_to_me=list(state.assigned_to_me), review_requested=list(state.review_requested))
    mr_state_json = json.dumps(mr_state)

    if not MR_STATE_FILE.parent.exists():
        MR_STATE_FILE.parent.mkdir(exist_ok=True, parents=True)

    with MR_STATE_FILE.open("w+") as fh:
        fh.write(mr_state_json)

def fetch_merge_requests(gl: gitlab.Gitlab) -> MRState:
    # Get the currently logged in user
    gl.auth()
    assert gl.user is not None, "Could not get the current user. Is the config file correct?"

    # Fetch the current IDs merge requests
    current_assigned_to_me: set[int] = {mr.attributes["id"] for mr in gl.mergerequests.list(iterator=True, scope="assigned_to_me", approved="no", state="opened")}
    current_review_requested: set[int] = {mr.attributes["id"] for mr in gl.mergerequests.list(iterator=True, reviewer_id=gl.user.id, scope="all", approved="no", state="opened")}

    return MRState(current_assigned_to_me, current_review_requested)

def get_merge_request_status():
    gl = get_gitlab_instance()

    upstream_state = fetch_merge_requests(gl)
    local_state = read_mr_state()
    new_local_state = MRState(local_state.assigned_to_me & upstream_state.assigned_to_me, local_state.review_requested & upstream_state.review_requested)

    write_mr_state(new_local_state)

    unseen_assigned_to_me = upstream_state.assigned_to_me - local_state.assigned_to_me
    unseen_review_requested = upstream_state.review_requested - local_state.review_requested


    output = dict(
        assigned_to_me=len(upstream_state.assigned_to_me),
        review_requested=len(upstream_state.review_requested),
        new_review_requested=len(unseen_review_requested),
        new_assigned_to_me=len(unseen_assigned_to_me),
    )

    return output

def mark_all_as_seen():
    gl = get_gitlab_instance()
    current_state = fetch_merge_requests(gl)
    write_mr_state(current_state)

@click.group()
def cli():
    pass

@cli.command("json")
def print_json():
    """ Prints the Merge Request data as raw JSON data """
    click.echo(json.dumps(get_merge_request_status()))

@cli.command("print")
@click.option("-N", "--on-new", type=str, help="Command to run if there are new merge requests")
def pretty_print(on_new: str | None):
    """ Pretty print the Review and Assignee statuses """
    status = get_merge_request_status()
    click.echo(f"Reviews: {status['review_requested']} ({status['new_review_requested']}) | Assigned: {status['assigned_to_me']} ({status['new_assigned_to_me']})")
    if on_new and (status["new_assigned_to_me"] > 0 or status["new_review_requested"] > 0):
        new_env = os.environ
        new_env.update({k: str(v) for k, v in status.items()})
        subprocess.run(["sh", "-c", on_new], env=new_env)

@cli.command("open")
def open_mr_view():
    """ Opens the GitLab Merge Request Dashboard using 'xdg-open' """
    gl = get_gitlab_instance()
    gl.auth()
    if gl.user is None:
        click.echo("GitLab instance user is 'None'!")
        sys.exit(1)
    mr_url = gl.url + f"/dashboard/merge_requests?reviewer_username={gl.user.attributes['username']}"
    run(["xdg-open", mr_url], check=True)

@cli.command("mark-seen")
def mark_seen():
    """ Mark all the Merge Requests as seen """
    mark_all_as_seen()

if __name__ == "__main__":
    cli()
