NAME=$1
USERNAME=$2
shift 2
URIS=$(printf '"%s\",' $@)
URIS=${URIS%,}

# Generate a new password
NEW_PASSWORD=$(bw generate -ulns --length 32)

# Fetch the JSON templates from Bitwarden
ITEM_TEMPLATE=$(bw get template item) 
LOGIN_TEMPLATE=$(bw get template item.login)


# Process the login entry
LOGIN=$(echo $LOGIN_TEMPLATE | jq ".username=\"$USERNAME\" | .password=\"$NEW_PASSWORD\"")

# Add the login and name to the new entry
ENTRY=$(echo $ITEM_TEMPLATE | jq ".name=\"$NAME\" | .login=$LOGIN")

# Create the new entry
echo $ENTRY | bw encode | bw create item

echo "New entry created with user: $USERNAME and password: $NEW_PASSWORD"
