#!/bin/bash

if command -v light-locker-command &> /dev/null; then
    light-locker-command -l
    # It may be installed, but still fail.
    if [ $? -eq 0 ]; then
        exit 0
    fi
fi

if command -v ffmpeg &> /dev/null; then
    if ! $HOME/.config/scripts/blur_lock.py; then
        i3lock -c 282828
    fi
else
    i3lock -c 282828
fi
