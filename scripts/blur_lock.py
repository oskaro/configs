#!/usr/bin/env python3

from dataclasses import dataclass
from subprocess import run, PIPE
import re
import os


@dataclass
class Screen:
    width: int = 0
    height: int = 0
    width_offset: int = 0
    height_offset: int = 0
    primary: bool = False

def screen_from_string(screen_string: str) -> Screen :
    matches = re.search(r"(\d+)x(\d+)\+(\d+)\+(\d+)", screen_string)

    if not matches:
        raise RuntimeError(f"could not find screen in {screen_string}")

    width, height, width_offset, height_offset = matches.group(1, 2, 3, 4)

    return Screen(
        int(width),
        int(height),
        int(width_offset),
        int(height_offset),
        "primary" in screen_string,
    )

def get_screens() -> list[Screen]:
    ret = run(["xrandr"], stdout=PIPE, check=True)
    ret = ret.stdout.decode('utf-8').split('\n')
    screen_lines = filter(lambda s: " connected " in s, ret)

    return list(map(screen_from_string, screen_lines))

def calculate_total_resolution(screens: list[Screen]) -> tuple[int,int]:
    height = 0
    width = 0
    for screen in screens:
        height = max(height, screen.height + screen.height_offset)
        width = max(width, screen.width + screen.width_offset)

    return (width, height)

def calculate_overlay_center(primary_screen: Screen) -> str:
    width, height = primary_screen.width, primary_screen.height
    width_offset, height_offset = primary_screen.width_offset, primary_screen.height_offset

    return f"{width_offset}+({width}-overlay_w)/2:{height_offset}+({height}-overlay_h)/2"


screens = get_screens()
width, height = calculate_total_resolution(screens)
overlay_str = calculate_overlay_center(screens[0])

FILTER = f"boxblur=5:1,overlay={overlay_str}"

TMPBG="/tmp/screen.png"
LOCK=os.environ["HOME"] + "/.config/images/lock.png"

run([
    "ffmpeg",
    "-f",
    "x11grab",
    "-video_size",
    f"{width}x{height}",
    "-y",
    "-i", os.environ["DISPLAY"],
    "-i", LOCK,
    "-filter_complex", FILTER,
    "-vframes","1",
    TMPBG],
    check=True
)

run(["i3lock", "-i", TMPBG], check=True)
