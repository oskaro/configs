CURRENT_DEVICE=$(pactl info | grep "Default Sink" | cut -d":" -f2 | tr --delete " ")
COMMAND=$1

case $1 in
    up)
        pactl set-sink-mute $CURRENT_DEVICE false;
        pactl set-sink-volume $CURRENT_DEVICE +5%
        ;;
    down)
        pactl set-sink-mute $CURRENT_DEVICE false;
        pactl set-sink-volume $CURRENT_DEVICE -5%
        ;;
    mute-toggle)
        pactl set-sink-mute $CURRENT_DEVICE toggle;
        ;;
esac

