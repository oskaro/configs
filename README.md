# My dotfiles

This is the repository where I keep my dotfiles, scripts, etc., shared across my machines.

The files in `dotfiles` corresponds to the files found in `$HOME` and should be symlinked in
there.

The `scripts` folder is assumed to be symlinked to `$HOME/.config/scripts/` in all the other
dotfiles, so make sure it is correctly placed.

## Ansible

To quickly set up a machine use the scripts found in [the ansible folder](./ansible).

