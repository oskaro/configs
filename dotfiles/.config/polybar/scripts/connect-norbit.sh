#!/usr/bin/env bash
if [[ -n $(pgrep -a openvpn$) ]]; then
    nmcli con down id NorbitVPN
else
    nmcli con up id NorbitVPN
fi
