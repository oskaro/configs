#!/usr/bin/env sh

echo "Killing old polybar processes"
killall -q polybar

echo "Waiting for polybar to shut down"

while pgrep -u $UID -x polybar > /dev/null; do sleep 1; done

echo "Polybar shut down"


echo "Launching polybar"
if [ "$HOSTNAME" = "archtop" ]; then
    polybar --reload main &
    for m in $(xrandr --query | grep -e " connected" | cut -d" " -f1 | grep -ve "eDP-1"); do
        MONITOR=$m polybar --reload secondary &
    done
else
    polybar --reload work-secondary &
    polybar --reload work &
fi
