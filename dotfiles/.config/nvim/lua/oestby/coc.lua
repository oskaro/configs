-- ##################################
-- # CoC Settings
-- ##################################

local function noremap(args, from, to, options)
    options["noremap"] = true
    vim.keymap.set(args, from, to, options)
end

-- I use the following CoC plugins:
vim.g.coc_global_extensions = {
    'coc-pyright',
    'coc-jedi',
    'coc-rust-analyzer',
    'coc-git',
    'coc-clangd',
    'coc-cmake',
    'coc-svelte',
    'coc-tsserver',
    'coc-vimtex',
    'coc-go',
    'coc-docker',
    'coc-lua',
    'coc-json',
    'coc-bitbake',
}

-- Use auocmd to force lightline update.
vim.api.nvim_create_autocmd("User",
    {
        pattern={"CocStatusChange","CocDiagnosticChange"},
        callback=function() vim.call("lightline#update") end
    }
)

function CheckBackSpace()
  local col = vim.fn.col('.') - 1
  local line = vim.fn.getline('.')
  local is_whitespace = line:sub(col, col):match('%s') ~= nil
  return col == 0 or is_whitespace
end

vim.keymap.set("i", "<TAB>", function()
    if vim.call("coc#pum#visible") ~= 0 then
        return vim.call("coc#pum#next", 1)
    elseif CheckBackSpace() then
        return "<Tab>"
    else
        return vim.call("coc#refresh")
    end
end,
{
    expr=true,
    noremap=true,
    silent=true
})
vim.keymap.set("i", "<S-TAB>", function()
    if vim.call("coc#pum#visible") ~= 0 then
        return vim.call("coc#pum#prev", 1)
    else
        return "<C-h>"
    end
end, {silent=true, expr=true, noremap=true})

-- This needs to be a vim command, not a Lua function to not break AutoPairs
vim.keymap.set("i", "<CR>",
[[coc#pum#visible() ? coc#pum#confirm() : "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"]],
{
    expr=true,
    silent=true,
    noremap=true,
    replace_keycodes=true
})


vim.keymap.set({"n", "o", "v"}, "<leader>gb", function()
    vim.call("coc#config", 'git.addGBlameToVirtualText', true)
end,
{
    noremap=true
})

local function ShowDocumentationOrScroll()
  if vim.fn.CocAction('hasProvider', 'hover') then
    if vim.fn["coc#float#has_float"]() == 1 then
        vim.fn["coc#float#scroll"](0)
    else
        vim.fn.CocActionAsync('doHover')
    end
  else
    vim.fn.feedkeys('K', 'in')
  end
end

local function ReverseScrollDocumentation()
    if vim.fn["coc#float#has_float"]() == 1 then
        vim.fn["coc#float#scroll"](1)
    else
        vim.fn.feedkeys('J', 'in')
    end
end


-- Use K to show documentation in preview window.
vim.keymap.set("n", "K", ShowDocumentationOrScroll, {
    silent=true,
    noremap=true
})

vim.keymap.set("n", "J", ReverseScrollDocumentation,
{
    silent=true,
    noremap=true
})


vim.api.nvim_create_autocmd({"CursorHold"}, {pattern="*", callback=function() vim.fn.CocActionAsync('highlight') end})

noremap("i", "<c-space>", function() return vim.call("coc#refresh") end, {silent=true, expr=true})

vim.keymap.set("n", "gy", "<Plug>(coc-type-definition)")
vim.keymap.set("n", "gi", "<Plug>(coc-implementation)")
vim.keymap.set("n", "gr", "<Plug>(coc-references)")
vim.keymap.set("n", "gd", "<Plug>(coc-definition)")

-- Symbol renaming.
vim.keymap.set("n", "<leader>rn", "<Plug>(coc-rename)")

-- TODO fix this
-- xmap <silent> <leader>a :<C-u> execute 'CocCommand actions.open ' .
-- visualmode()<CR>
vim.keymap.set("x", "<leader>a ", "<Plug>(coc-codeaction-selected)")
vim.keymap.set("n", "<leader>a ", "<Plug>(coc-codeaction-selected)")

-- Formatting selected code.
vim.keymap.set("x", "<leader>f ", "<Plug>(coc-format-selected)")
vim.keymap.set("n", "<leader>f ", "<Plug>(coc-format-selected)")

vim.keymap.set("x", "if", "<Plug>(coc-funcobj-i)")
vim.keymap.set("o", "if", "<Plug>(coc-funcobj-i)")
vim.keymap.set("x", "af", "<Plug>(coc-funcobj-a)")
vim.keymap.set("o", "af", "<Plug>(coc-funcobj-a)")
vim.keymap.set("x", "ic", "<Plug>(coc-classobj-i)")
vim.keymap.set("o", "ic", "<Plug>(coc-classobj-i)")
vim.keymap.set("x", "ac", "<Plug>(coc-classobj-a)")
vim.keymap.set("o", "ac", "<Plug>(coc-classobj-a)")

-- vim.opt.shortmess ("c")

-- ##################################
-- CoC Language specific configs
-- ##################################

vim.opt.hidden = true
-- CoC does not expand $HOME in the coc-config.json file, so we call the
-- settings here
local function file_exists(path)
    local f = io.open(path, "r")
    if f ~= nil then
        io.close(f)
        return true
    else
        return false
    end
end

local clangd_path = os.getenv("HOME") .. "/.config/coc/extensions/coc-clangd-data/install/13.0.0/clangd_13.0.0/bin/clangd"

if not file_exists(clangd_path) then
    clangd_path = vim.fn.exepath("clangd")
end
vim.call("coc#config", "clangd.path", clangd_path)

vim.call("coc#config", "rust-analyzer.serverPath", os.getenv("HOME") .. "/.local/bin/rust-analyzer")

vim.g.rustfmt_autosave = 1
vim.g.rustfmt_emit_files = 1
vim.g.rustfmt_fail_silently = 0

-- VimTeX config
-- Set the PDF viewer to zathura
vim.g.vimtex_view_method = 'zathura'
vim.g.vimtex_quickfix_enabled = 0

-- Set some parameters for the latexmk compiler.
vim.g.vimtex_compiler_latexmk = {
    build_dir = 'build',
    options = {
       '-use-make',
       '--shell-escape',
    }
}
vim.g.vimtex_compiler_latexmk_engines = {
     _                      = '-lualatex',
     pdflatex               = '-pdf',
     dvipdfex               = '-pdfdvi',
     lualatex               = '-lualatex',
     xelatex                = '-xelatex',
     ["context (pdftex)"]   = '-pdf -pdflatex=texexec',
     ["context (luatex)"]   = '-pdf -pdflatex=context',
     ["context (xetex)"]    = "-pdf -pdflatex='texexec --xtx'"
}

-- Set the spellchecking language to British English
vim.opt.spelllang="en_gb,nb"
vim.opt.spellsuggest="best,9"
vim.opt.spell=true

vim.keymap.set({"n", "v", "o"}, "<leader>k ", "z=")
vim.keymap.set({"n", "v", "o"}, "<leader>g", "zg")
vim.keymap.set({"n", "v", "o"}, "<leader>G", "zg2")


vim.api.nvim_create_autocmd({"BufNewFile","BufRead"},
{
    pattern = "Jenkinsfile",
    command = "setf groovy"
})
vim.api.nvim_create_autocmd({"BufNewFile","BufRead"},
{
    pattern = "*.ksy",
    command = "setf yaml"
})

vim.api.nvim_create_autocmd({"BufNewFile","BufRead"},
{
    pattern = "*.{bb,bbclass,bbappend,in}",
    command = "setf bitbake"
})
