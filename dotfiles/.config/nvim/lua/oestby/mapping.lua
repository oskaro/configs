vim.g.mapleader = " "
local map = vim.keymap.set
-- Switching between splits
map("n", "<leader>J", "<C-W><C-J>")
map("n", "<leader>K", "<C-W><C-K>")
map("n", "<leader>L", "<C-W><C-L>")
map("n", "<leader>H", "<C-W><C-H>")

map("n", "<C-E>", function() vim.cmd("NERDTreeToggle") end, {desc="Toggle NERDTree"})


-- List out mappings
map("n", "<leader>m", vim.cmd.Maps, {desc = "Fuzzy search in normal mode mappings"})

-- Open hotkeys
map("n", "<leader>F", vim.cmd.Files, {desc="Search for files from the CWD", silent=true})
map("n", "<leader>f", function() vim.cmd.Files(vim.fn.expand("%:h")) end, {desc="Search in files from the current buffer", silent=true})
map("n", "<leader>;", vim.cmd.Buffers, {silent=true, desc = "Fuzzy search in all open buffers"})
map("n", "<leader>gl", vim.cmd.Commits, {silent=true, desc = "Fuzzy search in all commits in current working directory"})
map({"n"}, "<leader>S", vim.cmd.RG, {noremap=true, desc = "Search in files from current working directory"})
map({"v","o"}, "<leader>S", function() vim.cmd.RG(VSelection()) end, {noremap=true, desc = "Search in files from current working directory"})
map({"n"}, "<leader>s", vim.cmd.RGRel, {noremap=true, desc = "Search in files relative to current buffer"})
map({"v","o"}, "<leader>s", function() vim.cmd.RGRel(VSelection()) end, {noremap=true, desc = "Search in files relative to current buffer"})

map({"n","v","o"}, "H" ,"^")
map({"n","v","o"}, "L" ,"$")

-- Center search results
map({"n","n"}, "n", "nzz", {silent=true})
map({"n","n"}, "N", "Nzz", {silent=true})
map({"n","n"}, "*", "*zz", {silent=true})
map({"n","n"}, "#", "#zz", {silent=true})

-- Move content
-- This moves the current line up or down
map("n", "<C-K>", ":move .-2<CR>==")
map("n", "<C-J>", ":move .+1<CR>==")

-- Moves the current selection up or down, then reselects the previous selection
map("x", "<C-J>", ":move '>+1<CR>gv=gv", {silent=true})
map("x", "<C-K>", ":move '<-2<CR>gv=gv", {silent=true})

-- Left and right can switch buffers
map({"n","n"},"<left>", ":bp<CR>")
map({"n","n"},"<right>", ":bn<CR>")
map("n", "<leader>q", ":bdelete<CR>", {silent=true})

-- <leader><leader> toggles between buffers
map("n", "<leader><leader>", "<c-^>")

-- " Neat X clipboard integration
-- " ,p will paste clipboard into buffer
-- " ,c will copy entire buffer into clipboard
-- vim.keymap.set({"n", "o"}, "<leader>p", ":read !xsel --clipboard --output<cr>")
-- vim.keymap.set({"n", "o"}, "<leader>y", ":call ToClipboard(VSelection())<cr>")
map({"n", "o", "v"}, "<leader>p", [["+p]])
map({"n", "o", "v"}, "<leader>y", [["+y]])

