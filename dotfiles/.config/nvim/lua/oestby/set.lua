vim.opt.filetype = "off"
vim.opt.filetype.plugin = "on"

vim.g.mapleader= " "

-- Show line number and relative numbers
vim.opt.number=true
vim.opt.relativenumber=true
vim.opt.showmode=false
vim.opt.signcolumn="yes"

vim.opt.autoindent=true

vim.opt.cmdheight=2

vim.opt.updatetime=300

vim.opt.expandtab=true
vim.opt.shiftwidth=4
vim.opt.softtabstop=4

-- Enable a undolog
vim.opt.undodir=os.getenv("HOME") .. '/.vimdid'
vim.opt.undofile=true

-- Natural splitting
vim.opt.splitright=true
vim.opt.splitbelow=true

vim.opt.mouse="a"

if vim.fn.has('termguicolors') then
    vim.opt.termguicolors=true
end

if not vim.fn.has('gui_running') then
    vim.opt.t_Co=256
end

if not os.getenv('TERM') == "-256color" and not os.getenv('TERM') == "screen-256color" then
    -- screen does not (yet) support truecolor
    vim.opt.termguicolors=true
end

vim.g.base16colorspace=256
vim.opt.background="dark"
vim.cmd.colorscheme('base16-gruvbox-dark-medium')

-- Enable highlighting of trailing whitespaces
vim.g.better_whitespace_enabled=1
vim.g.strip_whitespace_on_save=0
vim.g.better_whitespace_ctermcolor='lightred'

local function region_to_text(region)
  local text = ''
  local maxcol = vim.v.maxcol
  for line, cols in vim.spairs(region) do
    local endcol = cols[2] == maxcol and -1 or cols[2]
    local chunk = vim.api.nvim_buf_get_text(0, line, cols[1], line, endcol, {})[1]
    text = ('%s%s\n'):format(text, chunk)
  end
  return text
end

function VSelection()
    local savereg = vim.fn.getreg('"')
    local savetype = vim.fn.getregtype('"')
    vim.cmd([[silent normal! gvy]])
    local line = vim.fn.getreg('"')
    vim.fn.setreg('"', savereg, savetype)
    return line
end

function ToClipboard(text)
    print(text)
    vim.fn.system("xsel -ib", text)
end

vim.g.vim_svelte_plugin_use_typescript = 1

-- Jump to last edit position on opening file
if vim.fn.has("autocmd") then
  -- https://stackoverflow.com/questions/31449496/vim-ignore-specifc-file-in-autocommand
    function Resume()
        if vim.fn.expand("%:p"):match("%m/.git/") == nil and vim.fn.line("'\"") <= vim.fn.line("$") then
            vim.fn.execute("normal! g'\"")
        end
    end
    vim.api.nvim_create_autocmd("BufReadPost", { pattern="*", callback = Resume})
   -- vim.cmd([[
   --   au BufReadPost * if expand('%:p') !~# '\m/\.git/' && line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
   -- ]])
end
