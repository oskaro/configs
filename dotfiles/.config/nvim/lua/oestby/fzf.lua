-- FZF configurations

-- Add hidden files to RG command
vim.api.nvim_create_user_command("RG",
function(opts)
     vim.fn["fzf#vim#grep2"]("rg --column --line-number --no-heading --color=always --hidden", opts.fargs[1] or "")
end,
{
    bang=true,
    nargs="?",
    desc="Do a grep search from the current working directory"
})

vim.api.nvim_create_user_command("RGRel",
function(opts)
     vim.fn["fzf#vim#grep2"](
        "rg --column --line-number --no-heading --color=always --hidden",
        opts.fargs[1] or "",
        {
            dir = vim.fn.expand("%:h")
        }
    )
end,
{
    bang=true,
    nargs="?",
    desc = "Do a grep search from the buffer directory"
})

