-- TODO there is something wrong here.
vim.g.LightlineFilename = function()
    if vim.fn.expand('%:t'):len() then
        return vim.fn.expand("%:t")
    else
        return  '[No Name]'
    end
end

 vim.g.LightlineGitBlame = function()
    local blame = vim.b.coc_git_blame or ""
    if vim.fn.winwidth(0) > 120 then
        return blame
    else
       return '[No Blame]'
    end
end

vim.g.lightline = {
    colorscheme = "deus",
    active = {
        left = { { 'mode', 'paste' },
        { 'readonly', 'filename', 'modified', 'cocstatus'  } },
        right = { { 'lineinfo' },
        { 'percent' },
        { 'fileformat', 'fileencoding', 'filetype' },
        { 'gitblame' } }
    },
    component_function = {
        filename = 'LightlineFilename',
        cocstatus = 'coc#status',
        gitblame = 'LightlineGitBlame'
    },
}
