-- ##################################
-- # Plugins
-- ##################################
Plug = vim.fn["plug#"]
vim.call("plug#begin", '~/.config/nvim/plugged')

Plug 'scrooloose/nerdtree'
Plug 'itchyny/lightline.vim'

-- LaTeX stuff
Plug 'lervag/vimtex'
-- CoC is a code completion tool with its own plugin system
Plug('neoclide/coc.nvim', {branch = 'release'})

-- Web dev:
Plug 'leafOfTree/vim-svelte-plugin'

-- https://thoughtbot.com/blog/modern-typescript-and-react-development-in-vim
Plug 'pangloss/vim-javascript'
Plug 'leafgarland/typescript-vim'
Plug 'peitalin/vim-jsx-typescript'
Plug('styled-components/vim-styled-components', {branch = 'main'})
Plug 'jparise/vim-graphql'


Plug 'rust-lang/rust.vim'
Plug 'cespare/vim-toml'
Plug 'stephpy/vim-yaml'
Plug 'plasticboy/vim-markdown'
Plug 'aklt/plantuml-syntax'

-- For fuzzy searching
Plug('junegunn/fzf', {dir = '~/.fzf', ['do']= './install --all' })
Plug 'junegunn/fzf.vim'

Plug 'jiangmiao/auto-pairs'

Plug 'chriskempson/base16-vim'

-- For robotframework
Plug 'mfukar/robotframework-vim'

-- For zig
Plug 'zig-lang/zig.vim'

-- Highlighting whitespace
Plug 'ntpeters/vim-better-whitespace'

vim.call("plug#end")
