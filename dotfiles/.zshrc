# Check which environment we run.
if [[ $HOST == *"norbit"* ]]; then
    WORK_COMPUTER=true
fi

if [[ $HOST == *"pop-os"* ]]; then
    WORK_COMPUTER=true
fi

# Some nice utility functions
add_to_path_if_exists() {
    test -d $1 && export PATH="$PATH:$1"
}

source_if_exists() {
    test -f $1 && source $1
}

# Path to your oh-my-zsh installation.
export ZSH="$HOME/.oh-my-zsh"

# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
if [ "$WORK_COMPUTER" = true ]; then
    ZSH_THEME="bira"
else
    ZSH_THEME="agnoster"
fi

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
#HIST_STAMPS="dd/mm/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load?
# Standard plugins can be found in ~/.oh-my-zsh/plugins/*
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(
    git
    virtualenv
    docker
    docker-compose
    httpie
    rust
    fnm
)

source $ZSH/oh-my-zsh.sh
source_if_exists $HOME/.cargo/env

# User configuration

# Preferred editor for local and remote sessions
if [[ -n $SSH_CONNECTION ]]; then
    export EDITOR='nvim'
else
    export EDITOR='nvim'
fi

if [ "$WORK_COMPUTER" = true ]; then
    fpath=(~/.zsh/completion $fpath)
    autoload -Uz compinit && compinit -i
fi

add_to_path_if_exists /usr/local/go/bin
add_to_path_if_exists $HOME/.local/bin
add_to_path_if_exists $HOME/opt/gnuarmemb/bin
add_to_path_if_exists $HOME/opt/flutter/bin

# Virtualenv wrapper
export WORKON_HOME="$HOME/.venvp3"
export VIRTUALENVWRAPPER_PYTHON=$(readlink -f /usr/bin/python)
export VIRTUALENV_PYTHON=$(readlink -f /usr/bin/python)
# Pyenv
export PYENV_ROOT="$HOME/.pyenv"
[[ -d $PYENV_ROOT/bin ]] && export PATH="$PYENV_ROOT/bin:$PATH"
eval "$(pyenv init -)"

if test -f /usr/local/bin/virtualenvwrapper.sh; then
    source /usr/local/bin/virtualenvwrapper.sh
elif test -f $HOME/.local/bin/virtualenvwrapper.sh; then
    source $HOME/.local/bin/virtualenvwrapper.sh
elif test -f /etc/profile.d/virtualenvwrapper.sh; then
    # Should already be sourced.
else
    echo "Could not source virtualenvwrapper, double check the config"
fi

# nnn
export NNN_USE_EDITOR=0
export NNN_NOTE=$HOME/.notes
export NNN_CONTEXT_COLORS='1234'

# ssh
export SSH_KEY_PATH="~/.ssh/rsa_id"

# Fixes weird autocomplete errors.
ZSH_DISABLE_COMPFIX=true


if [ "$WORK_COMPUTER" = true ]; then
else
    export ZEPHYR_TOOLCHAIN_VARIANT=gnuarmemb
    export GNUARMEMB_TOOLCHAIN_PATH="$HOME/opt/gcc-arm-none-eabi"
    export CMAKE_EXPORT_COMPILE_COMMANDS=y
    export CONFIG_GIT_PATH=/home/oskar/git/configs
    export WORK_BASE_PATH=/home/oskar/norbit
    export EXPRESSIF_TOOLCHAIN_PATH="$HOME/.espressif/tools/xtensa-esp32-s2-elf/esp-202r3-8.4.0/xtensa-esp32s2-elf"
    export PATH="$PATH:$$EXPRESSIF_TOOLCHAIN_PATH/bin"
    export IDF_PATH=$HOME/opt/esp-idf
fi


# For libvirt
export LIBVIRT_DEFAULT_URI=qemu:///system

# Source all aliases.
source $HOME/.zsh_alias

if command -v kubectl &>/dev/null; then
    source <(kubectl completion zsh)
fi
if command -v helm &>/dev/null; then
    source <(helm completion zsh)
fi
if command -v argocd &>/dev/null; then
    source <(argocd completion zsh)
fi

source_if_exists ~/.fzf.zsh

if command -v fnm &>/dev/null; then
    eval "$(fnm env)"
fi

if [ "$WORK_COMPUTER" = true ]; then
    export TERM=xterm-256color
fi

source_if_exists $HOME/.bitwardenrc
export GPG_TTY=$(tty)

function tmux_update_env() {
    if [ -n "$TMUX" ]; then
        eval "$(tmux show-environment -s)"
    fi
}

function precmd() {
    tmux_update_env
}
source_if_exists ~/.hcvault
# Fuck windows
export DOTNET_CLI_TELEMETRY_OPTOUT=1
