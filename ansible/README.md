# Ansible machine setup

To set up the computer, install `ansible`.
Then install the playbook dependencies using:

```bash
ansible-galaxy install -r requirements.yml
```

Then run the playbook like so:

```bash
ansible-playbook main.yml --ask-become-pass
```

`ansible` should now set up the machine.

To add or remove packages or dotfiles to manage
add them to [the config file.](./default.config.yml)

